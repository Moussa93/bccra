# This is a sample Python script.
import argparse
from statistics import mean

from project import utils
from project.cli_parser import CLIParser
from project.parser import RaceTime


def calculate_race_times():
    # Initializing the command-line parser in order to pull arguments.
    cli_parser = CLIParser()

    # This script can take input in two different ways:
    # 1 - From the CLI directly, with all race times (separated by a semicolon),
    # 2 - From a text file, where all the race times are dumped in a file (separated by a new line)
    race_times_from_file = cli_parser.uses_file

    # If a file location is passed via CLI, we need to parse the file and dump race times in a list
    if race_times_from_file:
        race_times = utils.get_race_times_from_file_location(cli_parser.file_location)
    else:
        # Otherwise, a simple string `split` will generate our list.
        race_times = utils.get_race_times_from_string(cli_parser.race_times)

    # Race Times needs at least 1 and no more than 50 elements
    number_of_race_times = len(race_times)
    if number_of_race_times <= 0 or number_of_race_times > 50:
        raise Exception("There must be between 1 and 50 times")

    # Retrieve the average minutes
    average_minutes_per_race = get_average_minutes(race_times)

    # Added some formatting so averages in the thousands can be easily read
    print(f"Average Race Time, in Minutes: {average_minutes_per_race:,}")


# Given a list of race time strings, return the average race time
def get_average_minutes(times):
    # Now that we have a list of race times,
    # We can initiate our RaceTime object with each value
    race_times = [RaceTime(race_time) for race_time in times]

    # Once the list is generated, the average can be calculated
    race_time_average = mean(race_time.get_race_time_in_minutes() for race_time in race_times)

    # Rounding up
    return utils.round_half_up(race_time_average)


if __name__ == '__main__':
    calculate_race_times()
