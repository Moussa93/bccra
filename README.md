# Coding Challenge

This project is my solution to the`Race Average` problem.

## Project Setup

### Virtual Environment
The development took place in a [virtual environment (`venv`)](https://docs.python.org/3/library/venv.html#creating-virtual-environments) running `python 3.9.15`.

### Local Dependencies
The only dependency needed is `pytest`, and it is installed by running the following command in the root folder:
```zsh
$ pip3 install -r build/requirements/requirements.txt
```

### Executing the program
There are two ways this program can be executed.
In the root directory of this project:

#### - Passing dates directly using the CLI:
```zsh
$ python3 race_time_calculator.py --race-times "08:01 AM, DAY 1"
```
```zsh
$ python3 race_time_calculator.py --race-times "08:01 AM, DAY 1; 08:03 AM, DAY 1;"
```

#### - Passing the path of a local file containing the dates:
For testing purposes, a file has been created in `samples/dates_separated_by_new_line` and can be used with the command below for testing
```zsh
$ python3 race_time_calculator.py --file-location "samples/dates_separated_by_new_line"
```

### Running Tests
Tests are executed using [pytest](https://docs.pytest.org/en/latest/) and its dependencies should already be installed from running the `pip3` command during earlier steps.

Run the entire test suite:
```zsh
$ pytest
```

Run a single test file:
```zsh
$ pytest tests/test_race_time_calculator.py
```