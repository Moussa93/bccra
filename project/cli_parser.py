import argparse

from argparse import ArgumentParser


# This parser helps retrieve, validate and sanitize
# Command Line arguments used to run this script.
class CLIParser(ArgumentParser):

    def __init__(self):
        super().__init__()
        parser = argparse.ArgumentParser(
            description="Python Script built to calculate race time based on user input."
                        "Race times can be entered in two different formats: "
                        "1: Using the --race-times argument, "
                        "2: Using the --file-location argument",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

        # Validation is handled further down, so `required=False` is OK in this case.
        parser.add_argument("--race-times",
                            dest="race_times",
                            help="Type in multiple race times, separated by a semicolon.",
                            required=False)
        parser.add_argument("--file-location",
                            dest="file_location",
                            help="Path of a file containing race times, with one race time per line.", required=False)

        # Set the arguments
        args = parser.parse_args()
        self.config = vars(args)

        # Assign variables on script execution
        self.race_times = self.config.get('race_times', None)
        self.file_location = self.config.get('file_location', None)

        # Run validation
        self.validate_arguments()

    def validate_arguments(self):
        # One of them is required...
        if not self.race_times and not self.file_location:
            self.error("Error - One of the following arguments is required: --race-times, --file-location")

        # Can't have both race times and a file location passed
        if self.race_times and self.file_location:
            self.error("Error - Only use one argument: --race-times, --file-location")

    def get_race_times(self):
        return self.race_times

    def get_race_times_file_location(self):
        return self.file_location

    @property
    def uses_file(self):
        return self.file_location is not None
