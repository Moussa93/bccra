import re
from datetime import datetime, timedelta

# Useful variables
MINUTES_PER_DAY = 1440
STR_P_TIME_FORMAT = "%I:%M %p"

# Regex to make validation of race times much simpler and easier
DAYS_GROUP = "days"
HOURS_GROUP = "hours"
MINUTES_GROUP = "minutes"
MERIDIEM_GROUP = "meridiem"

# RegExplained:
# [0-1][0-9]|2[0-3]) (hours): First number, if it starts with 0 has to be in the [1-9] range, if 1, within [0,2]
# [0-5][0-9] (minutes): Range between 0 and 59
# [AaPp][Mm] (meridiem): Accepts a wide range of AM / PM chars combination
# [1-9][0-9]? (days): Positive Integer between 1 and 99
RACE_TIME_REGEX_PATTERN = f"^(?P<{HOURS_GROUP}>0[1-9]|1[0-2]):(?P<{MINUTES_GROUP}>[0-5][0-9]) (?P<{MERIDIEM_GROUP}>[AaPp][Mm]), DAY (?P<{DAYS_GROUP}>[1-9][0-9]?)$"


class RaceTime(object):

    def __init__(self, race_time):
        # Some sanitation on initial race_time string processing
        # To ensure that where are no whitespaces, and time casing is consistent.
        self.race_time = race_time.upper().strip()
        self.matched_groups = race_time_parser.parse_race_time_from_string(race_time)

    @property
    def hours(self):
        return int(self.matched_groups[HOURS_GROUP])

    @property
    def minutes(self):
        return int(self.matched_groups[MINUTES_GROUP])

    @property
    def meridiem(self):
        return self.matched_groups[MERIDIEM_GROUP]

    @property
    def days(self):
        return int(self.matched_groups[DAYS_GROUP])

    @property
    def race_time_with_meridiem(self):
        return f"{self.hours}:{self.minutes} {self.meridiem}"

    def get_race_time_in_minutes(self):
        # Race start at 8:00AM, so this will be our default start time
        # Datetime is neat in this case, because it allows us to build our objects on the fly,
        # but also run calculations with timedelta.
        start_time = datetime.strptime("8:00 AM", STR_P_TIME_FORMAT)
        end_time = datetime.strptime(self.race_time_with_meridiem, STR_P_TIME_FORMAT)

        # In the dimension I'm familiar with, a race cannot be finished before it's started
        if self.days == 1 and end_time < start_time:
            raise Exception(f"The race cannot ends before it starts.")

        # This is done so that a full day's worth of minutes isn't added
        if self.days > 1:
            end_time += timedelta(days=self.days - 1)

        return (end_time - start_time).total_seconds() / 60


class RaceTimeParser(object):

    def parse_race_time_from_string(self, race_time):

        # A regex is used to validate that the formatting of the date is correct,
        # But also to facilitate the extraction of each component of the race time
        race_time_regex_pattern = re.compile(RACE_TIME_REGEX_PATTERN)
        matched_pattern = race_time_regex_pattern.match(race_time)

        if not matched_pattern:
            raise Exception(f"{race_time} is not a valid race time format.")

        return matched_pattern.groupdict()


race_time_parser = RaceTimeParser()
