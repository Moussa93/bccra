import math


# Rounds up, with .5 rounding up.
def round_half_up(to_round_up):
    return math.ceil(to_round_up) if (float(to_round_up) % 1) >= 0.5 else round(to_round_up)


# Splits the race time string passed as an argument into a list of strings
def get_race_times_from_string(race_times_string, split_character=';'):
    race_times_string = race_times_string.removesuffix(split_character)

    # Cleanup
    return [race_time_string.strip() for race_time_string in race_times_string.split(split_character)]


# Reads a given file location line by line, and dumps each line into a list
def get_race_times_from_file_location(file_location):
    with open(file_location) as race_times_file:
        race_times = race_times_file.read().splitlines()

    return race_times
