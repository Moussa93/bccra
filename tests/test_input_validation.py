import argparse
from os import path
from unittest import mock

import pytest

import race_time_calculator


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(
                file_location=path.abspath(path.join(__file__, "../test_files/t_race_times_count_over"))))
def test_input_file_too_many_race_times(mocked_call, capsys):
    with pytest.raises(Exception) as race_time_count_exception:
        race_time_calculator.calculate_race_times()  # Run the script

    assert "There must be between 1 and 50 times" == str(race_time_count_exception.value)


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(
                file_location=path.abspath(path.join(__file__, "../test_files/t_race_times_count_under"))))
def test_input_file_zero_race_times(mocked_call, capsys):
    with pytest.raises(Exception) as race_time_count_exception:
        race_time_calculator.calculate_race_times()  # Run the script

    assert "There must be between 1 and 50 times" == str(race_time_count_exception.value)

@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(race_times="07:59 AM, DAY 1;"))
def test_race_cannot_end_before_start_time(mocked_call, capsys):
    with pytest.raises(Exception) as race_cannot_end_before_start_exception:
        race_time_calculator.calculate_race_times()  # Run the script

    assert "The race cannot ends before it starts." == str(race_cannot_end_before_start_exception.value)