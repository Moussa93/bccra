import argparse
from unittest import mock

import race_time_calculator


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(race_times="08:01 AM, DAY 1; 12:41 AM, DAY 3"))
def test_race_time_average_calculator(mocked_call, capsys):
    # Path the call
    mocked_call.patch(race_time_calculator.get_average_minutes)

    race_time_calculator.calculate_race_times()  # Run the script

    # Make sure the average_minutes() is called
    mocked_call.assert_called_once()

    # Result should be 1,221
    calculated_race_time_output, _ = capsys.readouterr()  # Capture output
    assert "Average Race Time, in Minutes: 1,221" in calculated_race_time_output
