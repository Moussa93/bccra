import argparse
from os import path
from unittest import mock

import pytest

import race_time_calculator


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(race_times="08:01 AM, DAY 1; 08:02 AM, DAY 1"))
def test_cli_string_argument(mocked_call, capsys):
    race_time_calculator.calculate_race_times()  # Run the script
    calculated_race_time_output, _ = capsys.readouterr()  # Capture output
    assert "Average Race Time, in Minutes: 2" in calculated_race_time_output


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(
                file_location=path.abspath(path.join(__file__, "../test_files/t_cli_race_times"))))
def test_cli_file_location_argument(mocked_call, capsys):
    race_time_calculator.calculate_race_times()  # Run the script
    calculated_race_time_output, _ = capsys.readouterr()  # Capture output
    assert "Average Race Time, in Minutes: 2" in calculated_race_time_output


@mock.patch('argparse.ArgumentParser.parse_args', return_value=argparse.Namespace())
def test_cli_parser_with_no_arguments(mocked_call, ):
    with pytest.raises(SystemExit) as system_exit_run:
        race_time_calculator.calculate_race_times()

    assert system_exit_run.type == SystemExit


@mock.patch('argparse.ArgumentParser.parse_args',
            return_value=argparse.Namespace(race_times="08:01 AM, DAY 1; 08:02 AM, DAY 1",
                                            file_location="dummy_location"))
def test_cli_parser_with_two_arguments(mocked_call):
    with pytest.raises(SystemExit) as system_exit_run:
        race_time_calculator.calculate_race_times()

    assert system_exit_run.type == SystemExit
